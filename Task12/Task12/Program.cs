﻿using System;
using System.Collections.Generic;

namespace Task12
{
    class Program
    {
        public const int boardSize = 8;
        public static char[,] board = new char[boardSize, boardSize];
        public static (int, int) initPos = (0, 0);
        public static Stack<char[,]> history = new Stack<char[,]>();
        public static List<(int, int)> queenPositions;
        public static char[,] temp;
        public static (int, int) freeTile;


        static void Main(string[] args)
        {


            // Initialize board to empty
            for (int i = 0; i < boardSize; i++)
            {
                for (int j = 0; j < boardSize; j++)
                {
                    board[i, j] = 'E';
                }
            }

            Console.WriteLine(checkIfSolution(board, (2, 2)));

            // TODO - bruk metoden under til å sjekke hver sti. Ta vare på boards på en stack. Men husk å erstatte Qer med X, men samtidig åpne opp igjen X'ene når det poppes bakover. 
        }

        public static bool checkIfSolution(char[,] board2, (int,int) initPos) 
        {
            board2[initPos.Item1, initPos.Item2] = 'Q';
            queenPositions = GetQueenPositions(board2);
            board2 = FillTakenSpots(board, queenPositions);
            Console.WriteLine("Initial board: ");
            DrawBoard(board2);

            freeTile = FindFirstfreeTile(board);

            while (initPos.Item1 != -1)
            {
                board[initPos.Item1, initPos.Item2] = 'Q';
                queenPositions = GetQueenPositions(board);
                board = FillTakenSpots(board, queenPositions);
                DrawBoard(board);

                initPos = FindFirstfreeTile(board);
            }

            return GetQueenPositions(board).Count == boardSize;
            
               
        }

        public static char[,] FillTakenSpots(char[,] board, List<(int,int)> queenPositions)
        {
            char[,] temp = board;

            foreach((int,int) qPos in queenPositions)
            {
                int x = qPos.Item1;
                int y = qPos.Item2;


                for (int i = 0; i < boardSize; i++)
                {
                    for (int j = 0; j < boardSize; j++)
                    {
                        // Vertical and horizontal
                        if (!temp[i, j].Equals('Q') && (x == i || y == j))
                        {
                            temp[i, j] = 'X';
                        }

                        // Diagonal
                        if (!temp[i, j].Equals('Q') && ((-x+i == -y+j) || (x + y == i + j)))
                        {
                            temp[i, j] = 'X';
                        }
                    }
                }
            }
            return temp;
        }


        public static (int,int) FindFirstfreeTile(char[,] board)
        {
            for (int i = 0; i < boardSize; i++)
            {
                for (int j = 0; j < boardSize; j++)
                {
                    if (board[i, j].Equals('E'))
                    {
                        return (i, j);
                    }
                }
            }
            
            // If no free tile is found - return (-1, -1)
            return (-1, -1);
        }


        public static List<(int,int)> GetQueenPositions(char[,] board2)
        {
            List<(int, int)> queenPositions = new List<(int, int)>();

            for (int i = 0; i < boardSize; i++)
            {
                for (int j = 0; j < boardSize; j++)
                {
                    if(board2[i,j].Equals('Q'))
                    {
                        queenPositions.Add((i, j));
                    }
                }
            }

            return queenPositions;
        }



        public static void DrawBoard(char[,] board)
        {
            Console.WriteLine($"-------Queens:{GetQueenPositions(board).Count}---------------");
            for (int i = 0; i < boardSize; i++)
            {
                for (int j = 0; j < boardSize; j++)
                {
                    Console.Write($" {board[i,j]} ");
                }

                Console.WriteLine();
            }

            Console.WriteLine();
            Console.WriteLine();
        }
    }
}
